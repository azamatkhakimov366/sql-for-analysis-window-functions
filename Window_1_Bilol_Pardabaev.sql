
-- Creating a Common Table Expression (CTE) to rank customers based on total sales
WITH RankedCustomers AS (
  
-- Selecting customer details, channel, and calculating total sales
  SELECT
    c.cust_id,
    c.cust_first_name,
    c.cust_last_name,
    c.country_id,
    s.channel_id,
    ch.channel_desc,
    SUM(s.amount_sold) AS total_sales,
    
    -- Ranking customers based on their total sales
    RANK() OVER (ORDER BY SUM(s.amount_sold) DESC) AS sales_rank
  FROM
    customers c
    JOIN sales s ON c.cust_id = s.cust_id
    JOIN channels ch ON s.channel_id = ch.channel_id
  WHERE
    
  -- Filtering sales within the specified date range (1998-2001)
    s.time_id BETWEEN '1998-01-01'::DATE AND '2001-12-31'::DATE
  GROUP BY
    
  -- Grouping data by customer, channel, and country
    c.cust_id, c.cust_first_name, c.cust_last_name, c.country_id, s.channel_id, ch.channel_desc
)

-- Retrieving customer details, channel, and total sales for the top 300 ranked customers
SELECT
  rc.cust_id,
  rc.cust_first_name,
  rc.cust_last_name,
  rc.country_id,
  rc.channel_id,
  rc.channel_desc,
  ROUND(rc.total_sales, 2) AS total_sales
FROM
  RankedCustomers rc
WHERE
  
-- Selecting the top 300 customers based on their sales rank
  rc.sales_rank <= 300
ORDER BY
  
-- Ordering the results based on sales rank
  rc.sales_rank;
  